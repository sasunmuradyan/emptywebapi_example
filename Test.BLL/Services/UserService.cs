﻿using AutoMapper;
using Test.DAL;
using Test.DTO;
using Test.DAL.Entities;
using Test.BLL.Interfaces;
using System.Threading.Tasks;

namespace Test.BLL.Services
{
    public class UserService : ServiceBase<User,UserDto>,IUserService
    {
        public UserService(TestDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }
	}
}
