﻿using System;
using Test.BLL.Services;
using Test.BLL.Interfaces;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace Test.Bootstrap.Bootstrap
{
	public static class Injection
	{
		public static IServiceCollection RegisterServices(this IServiceCollection services)
		{
            services.AddScoped<IUserService, UserService>();
			return services;
		}
	}
}
