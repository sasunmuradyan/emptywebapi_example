﻿namespace Test.DTO
{
	public class UserDto: IDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
