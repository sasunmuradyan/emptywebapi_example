﻿namespace Test.DAL.Entities
{
	public class User: IEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
