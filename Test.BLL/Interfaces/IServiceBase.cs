﻿using System;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Test.BLL.Interfaces
{
	public interface IServiceBase<TEntity, TDto>
	{
		Task<bool> Any(int id);
		Task<List<TDto>> GetAll();
		Task<TDto> Get(int id);
		Task<TDto> Add(TDto model);
		Task<TDto> Update(TDto model);
		Task Remove(int id);
		Task<List<TDto>> Get(Expression<Func<TEntity, bool>> expression);
		Task<bool> Any(Expression<Func<TEntity, bool>> expression);
	}
}