﻿using Test.DAL;
using Test.DTO;
using AutoMapper;
using Test.BLL.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using System;

namespace Test.BLL.Services
{
	public class ServiceBase<TEntity, TDto> : IServiceBase<TEntity, TDto>
		where TEntity : class, IEntity
		where TDto : class, IDto
	{
		protected readonly TestDbContext _dbContext;
		protected readonly DbSet<TEntity> _dbSet;
		protected readonly IMapper _mapper;

		public ServiceBase(TestDbContext dbContext, IMapper mapper)
		{
			this._dbContext = dbContext;
			this._dbSet = _dbContext.Set<TEntity>();
			this._mapper = mapper;
		}

		public async Task<bool> Any(int id)
		{
			return await _dbSet.AnyAsync(x => x.Id == id);
		}

		public async Task<List<TDto>> GetAll()
		{
			var entity = await _dbSet.ToListAsync();
			var model = _mapper.Map<List<TDto>>(entity);
			return model;
		}

		public async Task<TDto> Get(int id)
		{
			var entity = await _dbSet.FindAsync(id);
			var model = _mapper.Map<TDto>(entity);
			return model;
		}

		public async Task<TDto> Add(TDto model)
		{
			var newEntity = _mapper.Map<TEntity>(model);
			var data = await _dbSet.AddAsync(newEntity);
			await _dbContext.SaveChangesAsync();
			var addedEntity = data.Entity;
			var newModel = _mapper.Map<TDto>(addedEntity);
			return newModel;
		}

		public async Task Remove(int id)
		{
			var entity = await _dbSet.FindAsync(id);
			_dbSet.Remove(entity);
			await _dbContext.SaveChangesAsync();
		}

		public async Task<TDto> Update(TDto model)
		{
			var entity = await _dbSet.FindAsync(model.Id);
			var newEntity = _mapper.Map(model, entity);
			var data = _dbSet.Update(newEntity);
			await _dbContext.SaveChangesAsync();
			var updatedEntity = data.Entity;
			var updatedModel = _mapper.Map<TDto>(updatedEntity);
			return updatedModel;
		}

		public async Task<List<TDto>> Get(Expression<Func<TEntity, bool>> expression)
		{
			var query = _dbSet.Where(expression);

			var entity = await query.ToListAsync();

			var model = _mapper.Map<List<TDto>>(entity);
			return model;
		}

		public async Task<bool> Any(Expression<Func<TEntity, bool>> expression)
		{
			var exists = await _dbSet.AnyAsync(expression);
			return exists;
		}
	}
}